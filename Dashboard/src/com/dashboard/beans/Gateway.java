package com.dashboard.beans;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.mysql.jdbc.Driver;




	
	
public class Gateway
{

	public static Connection connection()
	{
		Connection conn = null;
		
		try {
			
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost/boutique";
			conn = DriverManager.getConnection(url, "root", "");
			
		} catch ( SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return conn;
	}
	
	public static Admin getAdmin(String username , String password)
	{
		Connection conn = Gateway.connection();
		Admin admin = new Admin();
		
		
		try {
			String sql = "select * from `admin` where username = ? and password= ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next())
			{
				admin.setId(rs.getInt(1));
				admin.setUsername(rs.getString(2));
				admin.setPassword(rs.getString(3));
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return admin;
	}
	
	public static int insertProduct(Product p)
	{
		Connection conn = Gateway.connection();
		int result = 0;
		
		
		try {
			String  sql = "INSERT INTO `Produit`(`nom`, `desc`, `img`, `prix`, `idc`) VALUES (?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNom());
			ps.setString(2, p.getDesc());
			ps.setString(3, p.getImg());
			ps.setFloat(4, p.getPrix());
			ps.setInt(5, p.getIdc());
			
			result =  ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static int updateProduct(Product p)
	{
		Connection conn = Gateway.connection();
		int res = 0;
		
		
		try {
			String sql = "UPDATE `Produit` SET `nom`=?,`desc`=?,`img`=?,`prix`=?,`idc`=? WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, p.getNom());
			ps.setString(2, p.getDesc());
			ps.setString(3, p.getImg());
			ps.setFloat(4, p.getPrix());
			ps.setInt(5, p.getIdc());
			ps.setInt(6, p.getId());
			
			res = ps.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return res;
	}
	
	public static int deleteProduct(int id) 
	{
		Connection conn = Gateway.connection();
		int res = 0;
		try {
			String sql = "DELETE FROM `Produit` WHERE id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			res = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return res;

	}
	
	public static Product getProductbyId(int id)
	{
		Connection conn = Gateway.connection();
		Product p = new Product();
		
		
		try {
			String sql = "select * from `Produit` where id = ?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next())
			{
				p.setId(rs.getInt(1));
				p.setNom(rs.getString(2));
				p.setDesc(rs.getString(3));
				p.setImg(rs.getString(4));
				p.setPrix(rs.getFloat(5));
				p.setIdc(rs.getInt(6));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return p;
	}
	
	
	public static ArrayList<Category> getAllCategory()
	{
		Connection conn = Gateway.connection();
		ArrayList<Category> cat = new ArrayList<Category>();
		try {
			String sql = "SELECT * FROM `category`";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{	Category ca = new Category();
				ca.setId(rs.getInt(1));
				ca.setTitle(rs.getString(2));
				ca.setImg(rs.getString(3));
				cat.add(ca);
			}
			
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return cat;
	}
	
	public static ArrayList<Product> getAllProducts()
	{
		Connection conn = Gateway.connection();
		ArrayList<Product> list = new ArrayList<Product>();
		try {
			String sql = "SELECT * FROM `Produit`";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next())
			{	
				Product pro = new Product();
				pro.setId(rs.getInt(1));
				pro.setNom(rs.getString(2));
				pro.setDesc(rs.getString(3));
				pro.setImg(rs.getString(4));
				pro.setPrix(rs.getFloat(5));
				pro.setIdc(rs.getInt(6));
				
				list.add(pro);
			}
			
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
}
