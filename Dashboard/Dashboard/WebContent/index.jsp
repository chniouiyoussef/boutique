<%@page import="com.dahsborad.dao.Procces"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Dashborad</title>
  <link href="https://fonts.googleapis.com/css2?family=Baloo+2&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/34ccdadeec.js" crossorigin="anonymous"></script>
  <!-- Bootstrap core CSS -->
  <link href="./css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="./css/style.css" rel="stylesheet">
</head>
<body>

<div>

	<div class="container">
  			<div class="z-top" style="max-width: 60%; margin: auto; padding-top: 80px;">
  				<div class="card bg-light mb-3 " >
				 <h1 class="text-center">Login</h1>
				  <div class="card-body">
				  		
				  		<form method="post"  action="procces" enctype="multipart/x-www-form-urlencoded">
						 <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-2 col-form-label">Username :</label>
						    <div class="col-sm-10">
						      <input type="text" name="username" class="form-control" id="inputEmail3" />
						      
						    </div>
						  </div>
						  
	
				
						  <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-2 col-form-label">Password :</label>
						    <div class="col-sm-10">
						  	  <input type="password" name="password" class="form-control" id="inputEmail3" />
						    </div>
						  </div>
						  
						   <div class="form-group row">
						    <div class="col-sm-10">
						      <button type="submit" class="btn btn-primary">submit</button>
						    </div>
						  </div>
						 </form>
								 
				  </div>
  	
 		 	</div>
  				
  			
  			</div>
	</div>
	


</div>

  <script src="./jquery/jquery.min.js"></script>
  <script src="./js/bootstrap.bundle.min.js"></script>
  <script src="./js/mini.js" ></script>
</body>
</html>