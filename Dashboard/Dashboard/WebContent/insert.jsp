<%@page import="com.dahsborad.dao.Procces"%>
<%@page import="com.dashboard.beans.Category"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	
	ArrayList<Category> list = (ArrayList<Category>) request.getAttribute("mylist");

%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Dashborad admin</title>
  <link href="https://fonts.googleapis.com/css2?family=Baloo+2&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/34ccdadeec.js" crossorigin="anonymous"></script>
  <!-- Bootstrap core CSS -->
  <link href="./css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="./css/style.css" rel="stylesheet">
</head>
<body>
							<div class="second-nav">
								<div class="container">
									<nav class="navbar navbar-expand-lg navbar-light ">
									  <a class="navbar-brand" href="http://localhost:8888/Dashboard/home">Management></a>
									  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
									    <span class="navbar-toggler-icon"></span>
									  </button>
									  <div class="collapse navbar-collapse" id="navbarText">
									     <ul class="navbar-nav ml-auto">
									      <li class="nav-item" id="home">
									        <a class="nav-link" href="http://localhost:8888/Dashboard/products">Accueil</a>
									      </li>
									      <li class="nav-item" id="shop">
									        <a class="nav-link" href="http://localhost:8888/Dashboard/addpro">add products</a>
									      </li>

									      <li class="nav-item" id="conatct">
									        <a class="nav-link" href="http://localhost:8888/Dashboard/logout">Log out </a>
									      </li>
									     
									    </ul>
									  </div>
									</nav>
								</div>
</div>

<div>
	<div class="container">
  			<div class="z-top" style="margin-top: 50px;">
  				<div class="card bg-light mb-3 " >
				 
				  <div class="card-body">
				  
			
				  		<form method="get"  action="inscprod" enctype="multipart/form-data">
						  <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-2 col-form-label">Enter Product Name :</label>
						    <div class="col-sm-10">
						      <input type="text" name="nom" class="form-control" id="inputEmail3" />
						      
						    </div>
						  </div>
						  
	
				
						  
						  <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-2 col-form-label">Product description :</label>
						    <div class="col-sm-10">
						        <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3" ></textarea>
						    </div>
						  </div>
						  
						  <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-2 col-form-label">Enter Unit Price :</label>
						    <div class="col-sm-10">
						  	  <input type="text" name="prix" class="form-control" id="inputEmail3" />
						    </div>
						  </div>
						  
				
						   <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-2 col-form-label">select Image :</label>
						    <div class="col-sm-10">
						      <div class="custom-file">
							    <input type="file" name="file"  />
							   <!--  <label class="custom-file-label" for="inputGroupFile03">Choose file</label>  -->
							  </div>

						    </div>
						  </div>
						  
						   <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-2 col-form-label">select category :</label>
						    <div class="col-sm-10">
						       <select class="custom-select" name="category_id" id="inputGroupSelect01" >
						       		<% for(Category c : list){ %>
							    	<option value="<%=c.getId() %>"><%=c.getTitle() %></option>
							    	<%} %>
							  </select>
						      
						    </div>
						  </div>
						  
						   <div class="form-group row">
						    <div class="col-sm-10">
						      <button type="submit" class="btn btn-primary">submit</button>
						    </div>
						  </div>
						 </form>
								 
				  </div>
  	
 		 	</div>
  				
  			
  			</div>
	</div>
	


</div>

  <script src="./jquery/jquery.min.js"></script>
  <script src="./js/bootstrap.bundle.min.js"></script>
  <script src="./js/mini.js" ></script>
</body>
</html>