<%@page import="com.dahsborad.dao.Procces"%>
<%@page import="com.dashboard.beans.Product"%>
<%@page import="com.dashboard.beans.Category"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%

	ArrayList<Product> list = (ArrayList<Product>) request.getAttribute("list");

%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Dashborad admin</title>
  <link href="https://fonts.googleapis.com/css2?family=Baloo+2&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/34ccdadeec.js" crossorigin="anonymous"></script>
  <!-- Bootstrap core CSS -->
  <link href="./css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="./css/style.css" rel="stylesheet">
</head>
<body>
	
								
								<div class="second-nav">
								<div class="container-fluied">
									<nav class="navbar navbar-expand-lg  bg-primary">
									  <a class="navbar-brand" href="http://localhost:8888/Dashboard/home">Management></a>
									  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
									    <span class="navbar-toggler-icon"></span>
									  </button>
									  <div class="collapse navbar-collapse" id="navbarText">
									     <ul class="navbar-nav ml-auto">
									      <li class="nav-item" id="home">
									        <a class="nav-link" href="http://localhost:8888/Dashboard/products">Accueil</a>
									      </li>
									      <li class="nav-item" id="shop">
									        <a class="nav-link" href="http://localhost:8888/Dashboard/addpro">add products</a>
									      </li>

									      <li class="nav-item" id="conatct">
									        <a class="nav-link" href="http://localhost:8888/Dashboard/logout">Log out </a>
									      </li>
									     
									    </ul>
									  </div>
									</nav>
								</div>
</div>

<div>
	<div class="container">
  			<div class="z-top" style="margin-top: 50px;">
  			
						<h1 class="text-center">Available Products</h1>
					
								
								<br/>
								
								<br/>
								
								<table id="productsTable"  class="table ">
												
									<thead>					
										<tr>					
											<th>id</th>
											
											<th>nom</th>
											<th>Description</th>
											<th>Price</th>			
											<th>Edit</th>
											<th>Delete</th>
											
										</tr>					
									</thead>
									<% for(Product p : list){ %>
										<tr>
										<td><%=p.getId() %></td>
									
										<td>
											<%=p.getNom() %>
										</td>
										<td>
											<%=p.getDesc() %>
										</td>
										<td>
											<%=p.getPrix() %>
										</td>
										
										<td>
											<a href="updatepro?idp=<%=p.getId()%>"><button type="button" class="btn btn-primary">Edit</button></a>
											
										</td>
										<td>
											<a href="deleteProduct?idp=<%=p.getId()%>"><button type="button" class="btn btn-primary">Delete</button></a>
											
										</td>
										</tr>
										
									<%} %>
									
									<hr>
									
									<!-- Modal -->
							
									
									
												
								</table>
		
		


			</div>
	</div>

  <script src="./jquery/jquery.min.js"></script>
  <script src="./js/bootstrap.bundle.min.js"></script>
  <script src="./js/mini.js" ></script>
</body>
</html>